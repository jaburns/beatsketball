﻿using System;
using System.Collections.Generic;
using UnityEngine;

enum SoundKind
{
    Hat, Snare, OpenHat
}

static class SoundKindSets
{
    static public readonly SoundKind[] NO_SOUND = new SoundKind[] {};
    static public readonly SoundKind[] JUST_HAT = new SoundKind[] { SoundKind.Hat };
    static public readonly SoundKind[] OPEN_HAT = new SoundKind[] { SoundKind.OpenHat };
    static public readonly SoundKind[] HAT_AND_SNARE = new SoundKind[] { SoundKind.Hat, SoundKind.Snare };
}

interface Mover 
{
    Vector3 GetPosition(float musicTime);
    SoundKind[] GetSounds(float musicTime);
}

class BounceMover : Mover
{
    float nextSoundTime = 0.0f;
    Vector3 xzPos;

    public BounceMover(float createdTime, Vector3 xzPos)
    {
        this.xzPos = xzPos;
        nextSoundTime = Mathf.Ceil(createdTime * 4f) / 4f;
    }

    static float parabola(float x)
    {
        var x1 = x - 0.5f;
        return 1f - 4*x1*x1;
    }

    Vector3 Mover.GetPosition(float musicTime)
    {
        var t2 = parabola(musicTime % 1f);
        return new Vector3(xzPos.x, t2, xzPos.z);
    }

    SoundKind[] Mover.GetSounds(float musicTime)
    {
        if (musicTime >= nextSoundTime) {
            nextSoundTime += 0.25f;
            if (Mathf.Abs(musicTime - Mathf.Round(musicTime)) < 0.1f) {
                return SoundKindSets.HAT_AND_SNARE;
            } else {
                return SoundKindSets.JUST_HAT;
            }
        }
        return SoundKindSets.NO_SOUND;
    }
}

class CarryMover : Mover
{
    float nextSoundTime = 0.0f;

    public CarryMover(float createdTime)
    {
        nextSoundTime = Mathf.Ceil(createdTime * 8f) / 8f;
    }

    Vector3 Mover.GetPosition(float musicTime)
    {
        return OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch) + Vector3.back;
    }

    SoundKind[] Mover.GetSounds(float musicTime)
    {
        if (musicTime >= nextSoundTime) {
            nextSoundTime += 0.125f;
            return SoundKindSets.JUST_HAT;
        }
        return SoundKindSets.NO_SOUND;
    }
}

class TossMover : Mover
{
    const float GRAVITY = 5.0f;

    float nextSoundTime = 0.0f;
    Vector3 position;
    Vector3 velocity;

    float predictedLandTime;

    public TossMover(float createdTime, Vector3 position, Vector3 velocity)
    {
        var flatVel = velocity;
        flatVel.y = 0;
        var sinLaunchAngle = Mathf.Sin(Vector3.Angle(flatVel, velocity) * Mathf.Deg2Rad);
        var vs = velocity.magnitude * sinLaunchAngle;
        var airTime = (vs + Mathf.Sqrt(vs*vs + 2f*GRAVITY*position.y)) / GRAVITY;
        var landTime = createdTime + airTime;

        predictedLandTime = Mathf.Round(landTime);
        var newAirTime = predictedLandTime - createdTime;
        var newVelMag = (GRAVITY * newAirTime / 2f - position.y / newAirTime) / sinLaunchAngle;

        velocity = velocity.normalized * newVelMag;

        this.position = position;
        this.velocity = velocity;
        nextSoundTime = Mathf.Ceil(createdTime * 4f) / 4f;
    }

    Vector3 Mover.GetPosition(float musicTime)
    {
        velocity += GRAVITY * Vector3.down * Time.deltaTime;
        position += velocity * Time.deltaTime;
        return position;
    }

    SoundKind[] Mover.GetSounds(float musicTime)
    {
        if (musicTime >= nextSoundTime) {
            nextSoundTime += 0.25f;
            if (musicTime > predictedLandTime - 0.2) {
                return SoundKindSets.HAT_AND_SNARE;
            } else if (Mathf.Abs(musicTime - Mathf.Round(musicTime)) < 0.1f) {
                return SoundKindSets.OPEN_HAT;
            } else {
                return SoundKindSets.JUST_HAT;
            }
        }
        return SoundKindSets.NO_SOUND;
    }
}

[Serializable]
class SoundAndSourcePair 
{
    public SoundKind soundKind;
    public AudioSource audioSource;
}

static class SoundAndSourcePairExtensions
{
    static public AudioSource FindSource(this SoundAndSourcePair[] array, SoundKind kind)
    {
        foreach (var p in array) {
            if (p.soundKind == kind) {
                return p.audioSource;
            }
        }
        return null;
    }
}

public class BeatsketBallController : MonoBehaviour 
{
    [SerializeField] float beatsPerSecond = 1.0f;
    [SerializeField] float audioLatencyOffset = 0.0f;
    [SerializeField] SoundAndSourcePair[] sourcesForSounds;
    [SerializeField] GameObject basketball;

    Mover mover;
    List<SoundKind> soundsToPlay;
    Vector3 lastBallPos;

    void Awake()
    {
        mover = new BounceMover(0.0f, basketball.transform.position);
    }

    void Update () 
    {
        var simTime = Time.time * beatsPerSecond;

        basketball.transform.position = mover.GetPosition(simTime);
        foreach (var s in mover.GetSounds(simTime + audioLatencyOffset)) {
            sourcesForSounds.FindSource(s).Play();
        }

        if ((mover is BounceMover || mover is TossMover) && OVRInput.Get(OVRInput.RawButton.RHandTrigger)) {
            mover = new CarryMover(simTime + audioLatencyOffset);
        }
        else if (mover is CarryMover && !OVRInput.Get(OVRInput.RawButton.RHandTrigger)) {
            mover = new TossMover(simTime, basketball.transform.position, (basketball.transform.position - lastBallPos) / Time.deltaTime);
        }
        else if (mover is TossMover && basketball.transform.position.y < 0) {
            mover = new BounceMover(simTime + audioLatencyOffset, basketball.transform.position);
            basketball.transform.position = mover.GetPosition(simTime);
        }

        lastBallPos = basketball.transform.position;
	}
}